#Getting the latest python image
FROM python:3.11

#labels as key value pair
LABEL Maintainer="@Gogden"

ENV TERM=xterm-256color
#ENV TOKEN=<INSERT TOKEN>
#ENV ID=<INSERT CLIENT ID>

#Working Directory
WORKDIR /usr/app/src

#Copy source files
COPY requirements.txt *.py *.env ./
RUN pip install --upgrade pip
RUN apt-get update && apt-get install -y gcc pkg-config
RUN pip install -r requirements.txt

#CMD to start python
CMD ["python","-u","./main.py","--debug","--env"]