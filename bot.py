from typing import Any
import discord
from discord.flags import Intents
from custom_print import info_print, dbg_print, err_print
from translate import emoji_to_country_code,translate,detect_lang


def init_bot():
    intent = discord.Intents(members=True, messages=True, guilds=True, reactions=True, message_content=True)
    return TranslateBot(intents=intent)

class TranslateBot(discord.Client):
    def __init__(self, *, intents: Intents, **options: Any) -> None:
        super().__init__(intents=intents, **options)

    async def on_ready(self):
        info_print(f"{self.user} has connected to Discord!")

    async def on_reaction_add(self,reaction,user):
        dbg_print(f"Reaction added to message {reaction.message.id}:\nEmoji:\t\t{reaction.emoji}\nContent:\t{reaction.message.content}")
        target = emoji_to_country_code(str(reaction.emoji))

        if target is not None:
            source = detect_lang(reaction.message.content)
            if(source is None or source == 'un'):
                err_print("Unable to detect source language!")
                return
            if target == "us" or target == 'gb':
                target = "en"
            dbg_print(f"Translating a message from {source} to {target}!")
            tran_text = translate(str(reaction.message.content).lower(),source, target)
            dbg_print(f"Translated Text: {tran_text}")
            async for thread in reaction.message.channel.archived_threads():
                if thread.id == reaction.message.id:
                    dbg_print(f"Existing Thread found. Thread/Message ID: {thread.id}")
                    break
            else:
                dbg_print("No Existing Thread found. Creating new thread.")
                thread = await reaction.message.create_thread(name="Translation",auto_archive_duration=60)
            dbg_print(f"Sending translated message to discord thread {thread.id}.")
            await thread.send(content=tran_text)
            await thread.edit(archived=True)