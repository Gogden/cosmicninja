from termcolor import colored

"""Available text colors:
    black, red, green, yellow, blue, magenta, cyan, white,
    light_grey, dark_grey, light_red, light_green, light_yellow, light_blue,
    light_magenta, light_cyan."""
debug_color = "blue"
info_color = "green"
error_color = "red"
DEBUG = 50
INFO = 30
ERROR = 10
_loglevel = INFO


def set_loglevel(level:int):
    global _loglevel
    _loglevel = level

def get_loglevel()->int:
    global _loglevel
    return _loglevel

def configure_custom_print(debug:str="blue",info:str="green",error:str="red"):
    global debug_color
    global info_color
    global error_color
    debug_color = debug
    info_color = info
    error_color = error


def dbg_print(text:str):
    global DEBUG
    if _loglevel >= DEBUG:
        _color_print("[DEBUG]"+text,debug_color)

def info_print(text:str):
    global INFO
    if _loglevel >= INFO:
        _color_print("[INFO]"+text,info_color)

def err_print(text:str):
    global ERROR
    if _loglevel >= ERROR:
        _color_print("[ERROR]"+text,error_color)

def _color_print(text:str, color:str):
    print(colored(text, color))

def print_header():
    _color_print("⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣠⣤⣶⣶⣶⠶⠾⠿⠿⠷⣷⣶⣦⣤⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⢀⣤⣶⠄⠀⠀⠀⠀⠀⠀⠀⠀⣠⣴⣿⠿⠛⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠙⠻⢿⣷⣤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⣀⠀⠀⠀⠀\n" +
    "⠀⠀⣠⡾⢫⣿⠀⠀⠀⠀⠀⠀⢀⣴⣾⠟⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠻⣿⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣤⣶⣾⠿⠿⠛⠛⠛⠛⠿⢷⣦⡀\n" +
    "⠀⣰⡿⠁⢸⣿⠀⠀⠀⠀⠀⣠⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⡄⠀⠀⠀⠀⠀⠀⣀⣤⣾⡿⠟⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠹⣷\n" +
    "⢠⣿⠁⠀⢸⣿⠀⠀⠀⠀⣼⡿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣹⣿⣆⠀⢀⣠⣴⣾⠟⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣿\n" +
    "⢸⡇⠀⠀⢸⣿⠀⠀⠀⣸⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣾⠟⠛⢻⣿⣿⠛⢉⣠⣤⣴⣶⣶⣶⣶⣦⣤⣄⡀⠀⠀⠀⠀⠀⣠⣾⠟\n" +
    "⣿⡇⠀⠀⢸⣿⠀⠀⠀⣿⣿⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⡏⠀⡴⠋⠘⣿⡿⠛⠋⠉⠁⠀⠀⠀⠀⠈⠉⠛⠿⣿⣦⣄⢀⣴⡿⠃⠀\n" +
    "⢿⡇⠀⡄⠀⣿⡀⠀⢸⣿⠻⣿⣿⣦⣤⢤⣀⣀⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⣠⣤⣶⣶⣿⡇⠞⠁⠀⢉⣿⡟⠛⠻⠿⢿⣶⣦⣄⠀⠀⠀⠀⠀⠈⠙⠻⠿⠛⠁⠀⠀\n" +
    "⢸⣇⠀⢱⠀⣿⡇⠀⠸⣿⠀⣿⡿⣿⣿⣧⠀⠀⢀⣈⣽⣿⣿⣿⣶⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣫⣿⣷⣶⣿⣿⡿⠿⣧⠀⠀⠀⠀⠀⠙⠿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠈⣿⡄⠈⣆⢹⣧⣀⡄⣿⡇⣿⢱⠘⣏⠉⠀⠀⠘⠛⠛⠛⠛⣻⡿⢿⠋⠉⠉⠙⠻⢿⣦⡀⠀⠀⠀⣠⠞⠁⢠⣿⠃⠀⢹⣧⠀⠀⠀⠀⠀⠀⠙⣿⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
    "⠀⠘⣿⣤⣼⠿⠟⢻⣇⢻⣷⠹⡎⢧⣸⠀⠀⠀⠀⠀⠀⠀⣸⡟⠁⢸⠀⠀⠀⠀⠀⠀⢙⣿⣆⡠⠞⠁⠀⣠⣿⠏⠀⠀⠘⣿⡀⠀⠀⠀⠀⠀⠀⢻⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⣴⡾⠛⠉⠀⠀⣠⣾⣿⣼⣿⡏⠛⠢⣭⣄⡀⠀⠀⠀⠀⢠⡟⠀⠀⠈⣆⠀⠀⠀⠀⣀⡾⠞⠉⠀⠀⣀⣴⡿⠋⠀⠀⠀⠀⣿⡇⠀⠀⠀⠀⠀⠀⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠸⣯⠀⣀⣴⠿⡏⠁⠙⠛⠛⠿⢷⣶⣤⣄⣉⠉⠒⠒⠲⠤⠤⠤⠤⠤⠼⠷⠖⠒⠉⠁⠀⠀⢀⣤⣾⠿⠋⠀⠀⠀⠀⠀⠀⣿⡇⠀⠀⠀⠀⠀⠀⢸⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠻⠟⠙⣿⣄⠹⣄⠀⣦⡀⠀⠀⠈⠉⠛⠿⢿⣷⣶⣤⣀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣠⣴⣾⠿⠛⠁⠀⠀⠀⠀⠀⠀⠀⢠⣿⠇⠀⠀⠀⣀⣀⣤⣼⡿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⢰⣾⠛⣠⣼⠿⢻⣿⣷⣤⡀⠀⠀⠀⠀⠀⠉⠙⠻⠿⣷⣦⣤⣶⣶⡾⢿⣿⡛⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣤⣴⣾⠿⠟⠛⠉⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠙⠿⠻⣧⠀⠀⠻⣦⠙⢿⣷⣤⡀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠻⠿⠦⠄⢻⣷⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠛⠋⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⢹⣧⠀⠀⢹⣷⠀⠈⠙⢿⣷⣦⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢿⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⠻⣧⡾⠟⠁⠀⠀⣠⣿⡟⠙⠛⠿⣷⣦⣄⡀⠀⠀⠀⠀⠀⣠⡄⠙⢿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣰⣿⠋⠀⠀⠀⠀⠀⠈⠉⠉⠛⠒⠀⠀⣴⡏⠀⠀⠀⠈⠻⣷⣄⣠⣴⡶⣶⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣰⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣟⢿⣦⣄⠀⠀⠀⠈⠛⠿⡁⣠⡿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⣿⠃⠑⠦⢤⣀⣀⡀⠀⠀⠀⠀⠀⣀⣀⣘⣿⡀⠙⠿⣷⣄⠀⠀⠀⣠⠛⢿⣶⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣿⠏⠙⠦⢄⣀⣀⠀⠈⠉⠉⠉⠉⠉⣀⣀⡠⢿⣷⠀⡀⠈⠛⠿⠶⠿⠶⠶⠿⠛⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⡿⠀⠀⠀⠀⠈⠉⠉⠉⠉⠉⠉⠉⠉⠉⠀⠀⠀⠻⣿⡝⢢⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⢃⣤⣤⣀⡀⠀⣠⣾⣶⣶⣦⣤⣤⣤⣤⣤⣤⠀⠀⢙⣿⣦⡹⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⡿⠒⠒⠒⠤⢬⣽⡿⠋⠀⠈⠉⠉⠉⠛⢿⣦⣴⠒⣋⡥⠔⠻⣷⣵⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⡇⠀⠀⠀⢠⣾⡟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢿⣿⣀⠀⠀⠀⢈⣿⡧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n" +
    "⠀⠀⠀⠀⠀⠀⠀⠀⢿⣥⣿⣷⣶⣶⣶⣿⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⣻⠿⠿⠿⠛⠛⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀","magenta")
    _color_print(" ▄████████  ▄██████▄     ▄████████   ▄▄▄▄███▄▄▄▄    ▄█   ▄████████ ███▄▄▄▄    ▄█  ███▄▄▄▄        ▄█    ▄████████ ▀█████████▄   ▄██████▄      ███     \n" +
            "███    ███ ███    ███   ███    ███ ▄██▀▀▀███▀▀▀██▄ ███  ███    ███ ███▀▀▀██▄ ███  ███▀▀▀██▄     ███   ███    ███   ███    ███ ███    ███ ▀█████████▄ \n" +
            "███    █▀  ███    ███   ███    █▀  ███   ███   ███ ███▌ ███    █▀  ███   ███ ███▌ ███   ███     ███   ███    ███   ███    ███ ███    ███    ▀███▀▀██ \n" +
            "███        ███    ███   ███        ███   ███   ███ ███▌ ███        ███   ███ ███▌ ███   ███     ███   ███    ███  ▄███▄▄▄██▀  ███    ███     ███   ▀ \n" +
            "███        ███    ███ ▀███████████ ███   ███   ███ ███▌ ███        ███   ███ ███▌ ███   ███     ███ ▀███████████ ▀▀███▀▀▀██▄  ███    ███     ███     \n" +
            "███    █▄  ███    ███          ███ ███   ███   ███ ███  ███    █▄  ███   ███ ███  ███   ███     ███   ███    ███   ███    ██▄ ███    ███     ███     \n" +
            "███    ███ ███    ███    ▄█    ███ ███   ███   ███ ███  ███    ███ ███   ███ ███  ███   ███     ███   ███    ███   ███    ███ ███    ███     ███     \n" +
            "████████▀   ▀██████▀   ▄████████▀   ▀█   ███   █▀  █▀   ████████▀   ▀█   █▀  █▀    ▀█   █▀  █▄ ▄███   ███    █▀  ▄█████████▀   ▀██████▀     ▄████▀   \n" +
            "                                                                                            ▀▀▀▀▀▀                                                   \n", "cyan")