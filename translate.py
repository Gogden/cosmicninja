import argostranslate.package
import argostranslate.translate
import pathlib
import progressbar
import os
import pycld2 as cld2
import re
from custom_print import dbg_print,info_print,err_print

def install_argos_translate():
    argostranslate.package.update_package_index()
    available_packages = argostranslate.package.get_available_packages()
    bar = progressbar.ProgressBar(maxval=len(available_packages),widgets=[progressbar.Bar('=','[',']'), ' ', progressbar.Percentage()])
    bar.start()
    position = 0
    for package in available_packages:
        path_to_install = pathlib.Path("./packages/" + argostranslate.package.argospm_package_name(package) + ".argosmodel")
        if(not path_to_install.exists()):
            download_path = package.download()
            path_to_install = pathlib.Path("./packages/" + download_path.name)
            try:
                os.mkdir(path_to_install.parent)
            except:
                pass
            try:
                os.rename(download_path.as_posix(),path_to_install.absolute().as_posix())
            except:
                pass
        argostranslate.package.install_from_path(path_to_install)
        position += 1
        bar.update(position)
    bar.finish()

def translate( string:str, from_code:str, target_lang:str):
    return argostranslate.translate.translate(string, from_code, target_lang)

def detect_lang( string:str ):
    _,_,details = cld2.detect(string)
    return details[0][1]

def emoji_to_country_code( emoji:str ):
    try:
        country_code = emoji.encode('unicode-escape').decode('ASCII')
        country_code1 = int(country_code[2:10],16) - 0x1f1a5
        country_code2 = int(country_code[12:],16) - 0x1f1a5
        country_code = chr(country_code1).lower() + chr(country_code2).lower()
        if bool(re.match("^[a-z][a-z]$",country_code)):
            return country_code
        else:
            err_print("Failed to find country code!")
            return None
    except Exception as e:
        err_print(f"Exception {e} thrown trying to convert emoji to country code!")
        return None